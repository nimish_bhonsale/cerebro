'use strict';

angular
    .module('myApp.services')
    .factory('ConfigService', ConfigService);

function ConfigService() {
    function GetEndpoint() {
        return 'http://10.110.19.211:3000/';
    }

    function GetArtifactsBucket() {
        return 'http://10.110.19.211:3000/artifacts';
    }

    function GetArtifactUri(id) {
        return 'http://10.110.19.211:3000/artifacts/' + id;
    }
    var service = {};
    service.GetEndpoint = GetEndpoint;
    service.ArtifactBucket = GetArtifactsBucket;
    service.GetArtifactUri = GetArtifactUri;
    return service;
}